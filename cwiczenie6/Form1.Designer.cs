﻿namespace cwiczenie6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button btnOblicz;
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLiczbaOd = new System.Windows.Forms.TextBox();
            this.txtLiczbaDo = new System.Windows.Forms.TextBox();
            this.lbLista = new System.Windows.Forms.ListBox();
            btnOblicz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Liczba od";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Liczba do";
            // 
            // txtLiczbaOd
            // 
            this.txtLiczbaOd.Location = new System.Drawing.Point(95, 34);
            this.txtLiczbaOd.Name = "txtLiczbaOd";
            this.txtLiczbaOd.Size = new System.Drawing.Size(100, 20);
            this.txtLiczbaOd.TabIndex = 2;
            // 
            // txtLiczbaDo
            // 
            this.txtLiczbaDo.Location = new System.Drawing.Point(95, 60);
            this.txtLiczbaDo.Name = "txtLiczbaDo";
            this.txtLiczbaDo.Size = new System.Drawing.Size(100, 20);
            this.txtLiczbaDo.TabIndex = 3;
            // 
            // lbLista
            // 
            this.lbLista.FormattingEnabled = true;
            this.lbLista.Location = new System.Drawing.Point(39, 115);
            this.lbLista.Name = "lbLista";
            this.lbLista.Size = new System.Drawing.Size(156, 69);
            this.lbLista.TabIndex = 4;
            // 
            // btnOblicz
            // 
            btnOblicz.Location = new System.Drawing.Point(77, 86);
            btnOblicz.Name = "btnOblicz";
            btnOblicz.Size = new System.Drawing.Size(75, 23);
            btnOblicz.TabIndex = 5;
            btnOblicz.Text = "Oblicz";
            btnOblicz.UseVisualStyleBackColor = true;
            btnOblicz.Click += new System.EventHandler(this.btnOblicz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 226);
            this.Controls.Add(btnOblicz);
            this.Controls.Add(this.lbLista);
            this.Controls.Add(this.txtLiczbaDo);
            this.Controls.Add(this.txtLiczbaOd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLiczbaOd;
        private System.Windows.Forms.TextBox txtLiczbaDo;
        private System.Windows.Forms.ListBox lbLista;
    }
}


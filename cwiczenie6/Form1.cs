﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Napisać program w WinForms, który pobierze od użytkownika 2 liczby – liczba od i liczba do,
        // a następnie w liście (listBox) wyświetli liczby podzielne przez 3, nie wyświetli liczb podzielnych
        // przez 6 a przy każdej innej liczbie wypisze znak ‘-’.
        // Zmienić program, aby w przypadku liczb dla których obecnie wypisywany jest ‘-’
        // wypisywał ostatecznie ‘-’ gdy liczba ta jest parzysta i ‘+’ gdy jest nieparzysta.
        
        private void btnOblicz_Click(object sender, EventArgs e)
        {
            lbLista.Items.Clear(); // czyścimy listę
            int liczbaOd = int.Parse(txtLiczbaOd.Text);
            int liczbaDo = int.Parse(txtLiczbaDo.Text);

            for (int i = liczbaOd; i < liczbaDo; i++)
            {
                if (i % 6 == 0)
                {
                    continue;
                }
                if (i % 3 == 0)
                {
                    lbLista.Items.Add(i); // dodajemy element do wyświetlanej listy
                }
                else
                {
                    if (i % 2 == 0) // liczby parzyste są podzielne przez 2; czyli modulo 2 równe jest 0
                    {
                        lbLista.Items.Add("-");
                    }
                     else
                    {
                        lbLista.Items.Add("+");
                    }
                }
            }
        }
    }
}
